from map import *
from helicopter import *
from pynput import *
import time
import os
import json

TICK_SLEEP = 0.1
TREE_UPDATE = TICK_SLEEP * 500
FIRE_UPDATE = TICK_SLEEP * 750
CLOUDS_UPDATE = TICK_SLEEP * 1000
MAP_W, MAP_H = 20, 20

field = Map(MAP_W, MAP_H)
helico = Helicopter(MAP_W, MAP_H)
tick = 1


MOVES = {'a': (0, -1), 'd': (0, 1), 's': (1, 0), 'w': (-1, 0)}
# f - сохранение, g - восстановление


def process_key(key):
    global helico, tick, field
    c = key.char.lower()
    if c in MOVES.keys():
        dx, dy = MOVES[c][0], MOVES[c][1]
        helico.move(dx, dy)
    elif c == 'f':
        data = {'helicopter': helico.export_data(),
                'clouds': field.clouds.export_data(),
                'field': field.export_data(),
                'tick': tick}
        with open('level.json', 'w') as lvl:
            json.dump(data, lvl)
    elif c == 'g':
        with open('level.json', 'r') as lvl:
            data = json.load(lvl)
            helico.import_data(data['helicopter'])
            tick = data['tick'] or 1
            field.clouds.import_data(data['clouds'])
            field.import_data(data['field'])
    elif c == 'o':
        exit(0)


listener = keyboard.Listener(on_press=None, on_release=process_key)
listener.start()

while True:
    os.system('cls' if os.name == 'nt' else 'clear')
    field.process_helico(helico)
    helico.print_stats()
    field.print_map(helico)
    print('TICK', tick)
    tick += 1
    time.sleep(TICK_SLEEP)
    if tick % TREE_UPDATE == 0:
        field.generate_tree()
    if tick % FIRE_UPDATE == 0:
        field.update_fire(helico)
    if tick % CLOUDS_UPDATE == 0:
        field.clouds.update()
