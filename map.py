from clouds import *
from utils import *

# 🟩 🌲 🌊 🏥 🏤 🚁 ♥ 🔥 🛢 💭 ⚡ 🟥 🏆 ⬛
# 0 - поле
# 1 - дерево
# 2 - река
# 3 - госпиталь
# 4 - апгрейд-шоп
# 5 - огонь

CELL_TYPES = "🟩🌲🌊🏥🏤🔥"
TREE_BONUS = 100
TREE_PENALTY = 100
UPGRADE_COST = 1000
LIFE_COST = 2000


class Map:
    def __init__(self, w, h):
        self.w = w
        self.h = h
        self.cells = [[0 for _ in range(w)] for _ in range(h)]
        self.generate_forest(3, 10)
        for i in range(w * h // (2 * (w + h))):
            self.generate_river(w * h // (w + h))
        self.generate_up_shop()
        self.generate_hospital()
        self.clouds = Clouds(w, h)
        self.update_fire()

    def check_bounds(self, x, y):
        if x < 0 or y < 0 or x >= self.h or y >= self.w:
            return False
        return True

    def print_map(self, helico):
        print("⬛" * (self.w + 2))
        for ri in range(self.h):
            print("⬛", end='')
            for ci in range(self.w):
                cell = self.cells[ri][ci]
                if self.clouds.cells[ri][ci] == 1:
                    print('💭', end='')
                elif self.clouds.cells[ri][ci] == 2:
                    print('⚡', end='')
                elif helico.x == ri and helico.y == ci:
                    print('🚁', end='')
                elif 0 <= cell < len(CELL_TYPES):
                    print(CELL_TYPES[cell], end='')
            print("⬛")
        print("⬛" * (self.w + 2))

    def generate_river(self, l):
        rc = randcell(self.w, self.h)
        rx, ry = rc[0], rc[1]
        self.cells[rx][ry] = 2
        while l > 0:
            rc2 = randcell2(rx, ry)
            rx2, ry2 = rc2[0], rc2[1]
            if self.check_bounds(rx2, ry2):
                self.cells[rx2][ry2] = 2
                rx, ry = rx2, ry2
                l -= 1

    def generate_forest(self, r, mxr):
        for ri in range(self.h):
            for ci in range(self.w):
                if randbool(r, mxr):
                    self.cells[ri][ci] = 1

    def generate_tree(self):
        c = randcell(self.w, self.h)
        cx, cy = c[0], c[1]
        if self.check_bounds(cx, cy) and self.cells[cx][cy] == 0:
            self.cells[cx][cy] = 1

    def generate_up_shop(self):
        c = randcell(self.w, self.h)
        cx, cy = c[0], c[1]
        self.cells[cx][cy] = 4

    def generate_hospital(self):
        c = randcell(self.w, self.h)
        cx, cy = c[0], c[1]
        if self.cells[cx][cy] != 4:
            self.cells[cx][cy] = 3
        else:
            self.generate_hospital()

    def add_fire(self):
        c = randcell(self.w, self.h)
        cx, cy = c[0], c[1]
        if self.cells[cx][cy] == 1:
            self.cells[cx][cy] = 5

    def update_fire(self, helico):
        for ri in range(self.h):
            for ci in range(self.w):
                if self.cells[ri][ci] == 5:
                    helico.score -= TREE_PENALTY
                    self.cells[ri][ci] = 0
        for i in range(self.w // self.h * 10):
            self.add_fire()

    @staticmethod
    def game_over(score):
        print("⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛")
        print("")
        print("GAME OVER")
        print("YOUR SCORE IS 🏆 :", score)
        print("")
        print("⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛")
        exit(0)

    def process_helico(self, helico):
        cell = self.cells[helico.x][helico.y]
        d = self.clouds.cells[helico.x][helico.y]
        if cell == 2:
            helico.tank = helico.mxtank
        elif cell == 5 and helico.tank > 0:
            helico.tank -= 1
            helico.score += TREE_BONUS
            self.cells[helico.x][helico.y] = 1
        elif cell == 4 and helico.score >= UPGRADE_COST:
            helico.mxtank += 1
            helico.score -= UPGRADE_COST
        elif cell == 3 and helico.score >= LIFE_COST:
            helico.lives += 10
            helico.score -= LIFE_COST
        if d == 2:
            helico.lives -= 1
            if helico.lives == 0:
                self.game_over(helico.score)

    def export_data(self):
        return {'cells': self.cells}

    def import_data(self, data):
        self.cells = data['cells'] or [[0 for _ in range(self.w)] for _ in range(self.h)]
