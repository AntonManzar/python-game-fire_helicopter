from utils import *


class Helicopter(object):
    def __init__(self, w, h):
        self.w = w
        self.h = h
        rc = randcell(w, h)
        self.x = rc[0]
        self.y = rc[1]
        self.tank = 0
        self.mxtank = 1
        self.score = 0
        self.lives = 20

    def move(self, dx, dy):
        nx, ny = dx + self.x, dy + self.y
        if 0 <= nx < self.h and 0 <= ny < self.w:
            self.x, self.y = nx, ny

    def print_stats(self):
        print("🛢 ", self.tank, '/', self.mxtank, sep='', end=" | ")
        print("🏆", self.score, end=" | ")
        print("♥", self.lives, end=" | ")
        print()

    def export_data(self):
        return {'score': self.score,
                'lives': self.lives,
                'x': self.x,
                'y': self.y,
                'tank': self.tank,
                'mxtank': self.mxtank
                }

    def import_data(self, data):
        self.tank = data['tank'] or 0
        self.mxtank = data['mxtank'] or 1
        self.score = data['score'] or 0
        self.lives = data['lives'] or 20
        self.x = data['x'] or 0
        self.y = data['y'] or 0
